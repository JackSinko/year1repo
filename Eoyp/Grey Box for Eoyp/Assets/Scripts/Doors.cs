﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors : MonoBehaviour
{
    public Transform TeleportTo;
    public GameObject Player;
    public Animator DoorAnims;
    public Vector3 Offset;
    public Camera Main;

    private void OnTriggerEnter(Collider other)
    {
        DoorAnims.SetBool("Door", true);
        StartCoroutine(DoorTrigger());
    }
    IEnumerator DoorTrigger()
    {
        yield return new WaitForSeconds(0.55f);
        Player.transform.position = TeleportTo.position + Offset;
        DoorAnims.SetBool("Door", false);
        Main.transform.position = TeleportTo.parent.GetChild(0).position;
    }
}
