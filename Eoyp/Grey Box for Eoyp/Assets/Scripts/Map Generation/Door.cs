﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Relative {
    LEFT,
    UP,
    RIGHT,
    DOWN,
    NULL
};

public class Door {

    public GameObject doorVisualiser; //the door object - this will be the 'walkway' not the 'arch'
    public Room Room; //the room the door belongs to
    public Relative Relative; //where the door is in relation to the room
    public Door Connection = null; //the other side of the door (door in another room)
                                   //if Connection is null, the door has not be 'unlocked'

    public Door(Room room, Relative relative) {
        this.Room = room;
        this.Relative = relative;
    }

}
