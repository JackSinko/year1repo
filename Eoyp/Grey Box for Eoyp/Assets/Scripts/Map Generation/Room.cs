﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
room states:

 activated = false - the room can't be accessed
 activated = true - the room COULD be accessed
 a door is locked if activated = true
 unlocked = true - the activated room has been entered & generated

*/

public class Room {

    public int ID;
    public bool Activated { get; set; }
    public Vector3 Position;
    public GameObject roomVisualiser;
    public Dictionary<Relative, int> connectedRooms = new Dictionary<Relative, int>();

    public Room(int ID, Vector3 position) {
        this.ID = ID;
        this.Position = position;

        //create doors
        new Door(this, Relative.LEFT);
        new Door(this, Relative.UP);
        new Door(this, Relative.RIGHT);
        new Door(this, Relative.DOWN);

    }

    public List<Room> getActiveConnectedRooms() {
        List<Room> activeConnectedRooms = new List<Room>();
        if (MapGrid.Rooms[connectedRooms[Relative.UP]].Activated == true)
            activeConnectedRooms.Add(MapGrid.Rooms[connectedRooms[Relative.UP]]);
        if (MapGrid.Rooms[connectedRooms[Relative.DOWN]].Activated == true)
            activeConnectedRooms.Add(MapGrid.Rooms[connectedRooms[Relative.DOWN]]);
        if (MapGrid.Rooms[connectedRooms[Relative.LEFT]].Activated == true)
            activeConnectedRooms.Add(MapGrid.Rooms[connectedRooms[Relative.LEFT]]);
        if (MapGrid.Rooms[connectedRooms[Relative.RIGHT]].Activated == true)
            activeConnectedRooms.Add(MapGrid.Rooms[connectedRooms[Relative.RIGHT]]);
        return activeConnectedRooms;
    }

}