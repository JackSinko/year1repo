﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SeekTarget : IBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //causes agent to chase after the player
    public override BehaviourResult Execute(Enemy agent)
    {
        agent.gameObject.GetComponent<NavMeshAgent>().SetDestination(agent.Target.transform.position);

        return base.Execute(agent);
    }
}
