﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Composite : IBehaviour
{
    protected List<IBehaviour> Children;

    private void Awake()
    {
        Children = new List<IBehaviour>();
    }

    public void AddChild(IBehaviour newChild)
    {
        Children.Add(newChild);
    }
}
