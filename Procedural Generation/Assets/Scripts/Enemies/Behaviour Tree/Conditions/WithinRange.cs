﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WithinRange : IBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //returns true
    public override BehaviourResult Execute(Enemy agent)
    {
        if (Vector3.Distance(agent.gameObject.transform.position, agent.Target.transform.position) >= agent.AttackRange)
        {
            return BehaviourResult.SUCCESS;
        }
        return BehaviourResult.FAILURE;
    }
}


