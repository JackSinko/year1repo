﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BehaviourResult
{
    SUCCESS,
    FAILURE,
    ONGOING
}

public class IBehaviour : ScriptableObject
{
	virtual public BehaviourResult Execute(Enemy agent)
    {
        return BehaviourResult.SUCCESS;
    }
}
