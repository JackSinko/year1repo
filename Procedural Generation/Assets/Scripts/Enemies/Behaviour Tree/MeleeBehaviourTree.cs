﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeBehaviourTree : MonoBehaviour {

    private IBehaviour startBehaviour;
    private Enemy enemy;

    // Use this for initialization
    void Start ()
    {
        enemy = gameObject.GetComponent<Enemy>();
        
        Sequence sequence = ScriptableObject.CreateInstance<Sequence>();
        WithinRange withinRange = ScriptableObject.CreateInstance<WithinRange>();
        sequence.AddChild(withinRange);
        SeekTarget seekTarget = ScriptableObject.CreateInstance<SeekTarget>();
        sequence.AddChild(seekTarget);

        startBehaviour = sequence;
    }

    // Update is called once per frame
    void Update () {

        enemy.agent.destination = enemy.gameObject.transform.position;
        startBehaviour.Execute(enemy);

    }
}
