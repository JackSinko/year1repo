﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : Composite {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //will return success if any child returns success
    override public BehaviourResult Execute(Enemy agent) 
    {
        foreach(IBehaviour child in Children)
        {
            if(child.Execute(agent) == BehaviourResult.SUCCESS)
            {
                return BehaviourResult.SUCCESS;
            }
        }

        return BehaviourResult.FAILURE;
    }
}
