﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence : Composite {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //will return success if all children return success
    override public BehaviourResult Execute(Enemy agent)
    {
        foreach (IBehaviour child in Children)
        {
            if (child.Execute(agent) == BehaviourResult.FAILURE)
            {
                return BehaviourResult.FAILURE;
            }
        }

        return BehaviourResult.SUCCESS;
    }
}
