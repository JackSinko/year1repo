﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
//generic class that will let us attach a script to a class to determind what enemy it is
//takes in an enemy
//public class EnemyBaseClass<T> where T: EnemyBaseClass
//{
//    //creation
//    public GameObject GameObject;
//    //reference to a the script component
//    public T ScriptComponent;
    
//    //contructor
//    //Specify the name of the enemy
//    public EnemyBaseClass(string name)
//    {
//        //initialise the game object
//        GameObject = new GameObject(name);
//        //add the script component to the GameObject
//        //returning the refrence back to the script component field so it can be useable once we instantiate elsewhere
//        ScriptComponent = GameObject.AddComponent<T>();

//    }
//}

    public enum EnemyType
    {
        Melee,
        Ranged,
        Boss
    }

public class Enemy : MonoBehaviour
{
    //what each class will have that inherits "EnemyBaseClass"

    public float Speed;
    public NavMeshAgent agent;
    public EnemyType Type;
    public int MaxHealth;
    public int CurrentHealth;
    public int Damage;
    public float AttackSpeed;
    public GameObject Target;
    public float AttackRange;

    //using awake so MonoBehaviour call all awake scripts before start scripts
    //setting up all common enemy elements as we are using a start method in each enemy class
    void Awake()
    {
        if(GameObject.FindGameObjectWithTag("Player") != null)
        {
            Target = GameObject.FindGameObjectWithTag("Player");
        }
        agent = gameObject.GetComponent<NavMeshAgent>();
        agent.speed = Speed;
        CurrentHealth = MaxHealth;
    }

    //Place all unique values here that will be determined at instantiation
    public virtual void Initialize(int speed, Vector3 position)
    {
        Speed = speed;
        transform.position = position;
    }
    private void Update()
    {
    }

    public void Attack()
    {

    }
}


