﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class MapGeneration : MonoBehaviour {

    //the number of times new rooms will be generated
    //this is also the minimum number of rooms in one direction
    //eg. if 10; it is possible that only 10 rooms could be activated. Max of 441 rooms.

    public static int numberOfRoomBranches = 15;
    [Range(0, 3)] public int roomExpandConnectionLimit; //MAX IS 3

    public List<Room> expandedRooms = new List<Room>();
    public GameObject RoomPrefab;

    //a list of all active rooms
    public List<Room> activeRooms = new List<Room>();

    //function to get a random number (as the normal random function was duplicating the number sometimes)
    private static readonly System.Random random = new System.Random();
    private static readonly object syncLock = new object();
    public static int RandomNumber(int min, int max) {
        lock (syncLock) { // synchronize
            return random.Next(min, max);
        }
    }

    void Start() {

        //create map/grid of rooms:

        int mapLengthWidth = (numberOfRoomBranches + numberOfRoomBranches + 1); //eg if 10; 10 rooms in one direction + 10 rooms in other direction + centre room
        MapGrid grid = new MapGrid(mapLengthWidth);
        grid.Create();

        //begin activating rooms:

        MapGrid.Rooms[grid.MiddleRoom].Activated = true;
        ExpandRooms(MapGrid.Rooms[grid.MiddleRoom]);

        //create visualisers for rooms:

        for (int count = 1; count <= grid.Size; count++) {

            Room room = MapGrid.Rooms[count];

            if (room.Activated == true) {

                //create room objects
                room.RoomVisualiser = (GameObject)Instantiate(RoomPrefab);
                room.RoomVisualiser.transform.position = room.Position;

                activeRooms.Add(room);

            }

        }

        //create door connections:

        //for each activated room
        for (int r = 0; r < activeRooms.Count; r++) {
            Room room = activeRooms[r];

            //connect corresponding rooms doors or set door to inactive if no connection
            room.ConnectDoors();
        }

    }

    //using the start room, start activating rooms outward
    private void ExpandRooms(Room startRoom) {

        List<Room> newRooms = new List<Room>();

        for (int level = 0; level < numberOfRoomBranches; level++) {

            //deep copy rooms
            List<Room> expandableRooms = new List<Room>();
            for (int i = 0; i < newRooms.Count; i++)
                expandableRooms.Add(newRooms[i]);
            newRooms.Clear();

            //add start room
            if (level == 0)
                expandableRooms.Add(startRoom);

            for (int count = 0; count < expandableRooms.Count; count++) {

                Room room = expandableRooms[count];

                //make sure a room with no possible ways to expand, doesnt attempt to expand
                if (room.GetActiveConnectedRooms().Count <= roomExpandConnectionLimit) {

                    //number of rooms to expand to: (random 1,2,3 or 4, depending on connectedRooms)
                    int minimumRooms = 1;
                    int activeRooms = 0;
                    for (int l = 1; l <= MapGrid.Rooms.Count; l++) {
                        if (MapGrid.Rooms[l].Activated == true)
                            activeRooms++;
                    }
                    if (activeRooms >= numberOfRoomBranches)
                        minimumRooms = 0;
                    int roomsToActivate = RandomNumber(minimumRooms, (roomExpandConnectionLimit - room.GetActiveConnectedRooms().Count + 1));

                    if (roomsToActivate > 0) {

                        //for X number of rooms:
                        for (int rooms = 1; rooms <= roomsToActivate; rooms++) {

                            //pick random side (left,up,right,down | 25, 50, 75, 100)
                            Relative side = Relative.NULL;
                            do {
                                int randomNumberForSide = RandomNumber(0, 100);
                                if (randomNumberForSide < 25) {
                                    side = Relative.LEFT;
                                } else if (randomNumberForSide < 50) {
                                    side = Relative.UP;
                                } else if (randomNumberForSide < 75) {
                                    side = Relative.RIGHT;
                                } else if (randomNumberForSide < 100) {
                                    side = Relative.DOWN;
                                }
                            } while (MapGrid.Rooms[room.ConnectedRooms[side]].Activated == true); //ignore if room is already activated

                            //activate the room
                            MapGrid.Rooms[room.ConnectedRooms[side]].Activated = true;
                            newRooms.Add(MapGrid.Rooms[room.ConnectedRooms[side]]);

                        }
                    }

                }

            }
        }

    }

}
