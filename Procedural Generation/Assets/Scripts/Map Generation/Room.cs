﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
room states:

 activated = false - the room can't be accessed
 activated = true - the room COULD be accessed
 a door is locked if activated = true
 unlocked = true - the activated room has been entered & generated

*/

public class Room {

    public int ID;
    public bool Activated { get; set; }
    public Vector3 Position;
    public GameObject RoomVisualiser;
    public Dictionary<Relative, Door> Doors = new Dictionary<Relative, Door>();
    public Dictionary<Relative, int> ConnectedRooms = new Dictionary<Relative, int>();

    public Room(int ID, Vector3 position) {
        this.ID = ID;
        this.Position = position;

        //create doors
        Doors.Add(Relative.UP, new Door(this, Relative.UP));
        Doors.Add(Relative.RIGHT, new Door(this, Relative.RIGHT));
        Doors.Add(Relative.DOWN, new Door(this, Relative.DOWN));
        Doors.Add(Relative.LEFT, new Door(this, Relative.LEFT));

    }

    public List<Room> GetActiveConnectedRooms() {
        List<Room> activeConnectedRooms = new List<Room>();
        if (MapGrid.Rooms[ConnectedRooms[Relative.UP]].Activated == true)
            activeConnectedRooms.Add(MapGrid.Rooms[ConnectedRooms[Relative.UP]]);
        if (MapGrid.Rooms[ConnectedRooms[Relative.DOWN]].Activated == true)
            activeConnectedRooms.Add(MapGrid.Rooms[ConnectedRooms[Relative.DOWN]]);
        if (MapGrid.Rooms[ConnectedRooms[Relative.LEFT]].Activated == true)
            activeConnectedRooms.Add(MapGrid.Rooms[ConnectedRooms[Relative.LEFT]]);
        if (MapGrid.Rooms[ConnectedRooms[Relative.RIGHT]].Activated == true)
            activeConnectedRooms.Add(MapGrid.Rooms[ConnectedRooms[Relative.RIGHT]]);
        return activeConnectedRooms;
    }

    public void ConnectDoors() {

        //check each side if relative room is active (eg. if left door, is left connected room active?)
        if (MapGrid.Rooms[ConnectedRooms[Relative.UP]].Activated == false) {
            //set doorway to inactive (as player cannot go to an inactive room)
            RoomVisualiser.transform.Find("Archway_UP").gameObject.SetActive(false);
        } else {
            //connect both corresponding doors
            Door currentRoomsDoor = Doors[Relative.UP];
            Door connectedRoomsDoor = MapGrid.Rooms[ConnectedRooms[Relative.UP]].Doors[Relative.DOWN];

            connectedRoomsDoor.Connection = currentRoomsDoor;
            currentRoomsDoor.Connection = connectedRoomsDoor;
        }

        //repeat for other sides:

        if (MapGrid.Rooms[ConnectedRooms[Relative.DOWN]].Activated == false) {
            RoomVisualiser.transform.Find("Archway_DOWN").gameObject.SetActive(false);
        } else {
            Door currentRoomsDoor = Doors[Relative.DOWN];
            Door connectedRoomsDoor = MapGrid.Rooms[ConnectedRooms[Relative.DOWN]].Doors[Relative.UP];

            connectedRoomsDoor.Connection = currentRoomsDoor;
            currentRoomsDoor.Connection = connectedRoomsDoor;
        }

        if (MapGrid.Rooms[ConnectedRooms[Relative.LEFT]].Activated == false) {
            RoomVisualiser.transform.Find("Archway_LEFT").gameObject.SetActive(false);
        } else {
            Door currentRoomsDoor = Doors[Relative.LEFT];
            Door connectedRoomsDoor = MapGrid.Rooms[ConnectedRooms[Relative.LEFT]].Doors[Relative.RIGHT];

            connectedRoomsDoor.Connection = currentRoomsDoor;
            currentRoomsDoor.Connection = connectedRoomsDoor;
        }

        if (MapGrid.Rooms[ConnectedRooms[Relative.RIGHT]].Activated == false) {
            RoomVisualiser.transform.Find("Archway_RIGHT").gameObject.SetActive(false);
        } else {
            Door currentRoomsDoor = Doors[Relative.RIGHT];
            Door connectedRoomsDoor = MapGrid.Rooms[ConnectedRooms[Relative.RIGHT]].Doors[Relative.LEFT];

            connectedRoomsDoor.Connection = currentRoomsDoor;
            currentRoomsDoor.Connection = connectedRoomsDoor;
        }

    }

}