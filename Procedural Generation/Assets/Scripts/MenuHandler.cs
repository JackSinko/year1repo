﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuHandler : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject optionsMenu;
    public GameObject exitMenu;
    public GameObject titleText;
    public GameObject exitConfirmationText;

    bool muted = false;

	// Use this for initialization
	void Start () {
        mainMenu.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OptionsHandler()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }

    public void ExitHandler()
    {
        mainMenu.SetActive(false);
        exitMenu.SetActive(true);
        exitConfirmationText.SetActive(true);
    }

    public void MuteToggle()
    {
        muted = !muted;
        AudioListener.volume = muted ? 0 : 1;
        Debug.Log("Mute: " + muted);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void BackToMainMenu()
    {
        optionsMenu.SetActive(false);
        exitMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
