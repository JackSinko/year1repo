﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public int numberOfAffectingFactors = 3;

    //damage taken
    void OnCollisionEnter(Collision collisionInfo)
    {
        Player player = new Player();
        if (collisionInfo.collider.tag == "Enemy")
        {
            player.health = player.health - 1;
        }
    }
    //scale from 1-100
    public float getOverallScore() {
        return ((
            (health * healthWeighting) +
            (treasure * treasureWeighting)) / numberOfAffectingFactors) * 100;

    }

    //weighting values:
    protected int healthWeighting; //based on maxHealth AND health
    protected int treasureWeighting;
    protected int enemiesKilledWeighting;

    //stat values:
    public int maxHealth;
    public int health;
    public int treasure;
    public int enemiesKilled;

    //player data:
    public float Speed;

}