﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed = 3;
    public float rotSpeed = 3;
    float rot = 0f;
    public float gravity = 8;

    //float baseSpeed
    //{
    //    get { return speed; }
    //}

    int floorMask;
    float camRayLength = 100f;

    Vector3 moveDir = Vector3.zero;

    CharacterController controller;


    void Start()
    {
        controller = GetComponent<CharacterController>();
       
    }

    void Update()
    {
        Movement();
        GetInput();
        rotation();
        
    }

    void Movement()
    {
        bool movementBeingUsed = false;
       
        if (controller.isGrounded)
        {
            
            //standard 4 direction
            //Forward movement
            if (Input.GetKeyDown(KeyCode.W))
                {
                moveDir = Vector3.forward;
               // moveDir *= speed;
                movementBeingUsed = true;
                }
            if (Input.GetKey(KeyCode.S))
            {
                moveDir = Vector3.back;
               // moveDir *= speed;
                movementBeingUsed = true;
            }
            if (Input.GetKey(KeyCode.A))
            {
                moveDir = Vector3.left;
                //moveDir *= speed;
                movementBeingUsed = true;
            }
            if (Input.GetKey(KeyCode.D))
            {
                moveDir = Vector3.right;
                //moveDir *= speed;
                movementBeingUsed = true;
            }

            if (Input.GetKey(KeyCode.W) && (Input.GetKey(KeyCode.D)))
            {
                Vector3 diagUpRight = (Vector3.up + Vector3.right).normalized;
            }

            // when keys are lifted
            if (Input.GetKeyUp(KeyCode.W))
            {
                moveDir = new Vector3(0, 0, 0);
                movementBeingUsed = false;
            }
            if (Input.GetKeyUp(KeyCode.S))
            {
                moveDir = new Vector3(0, 0, 0);
                movementBeingUsed = false;
            }
            if (Input.GetKeyUp(KeyCode.A))
            {
                moveDir = new Vector3(0, 0, 0);
                movementBeingUsed = false;
            }
            if (Input.GetKeyUp(KeyCode.D))
            {
                moveDir = new Vector3(0, 0, 0);
                movementBeingUsed = false;
            }

            


            ////8 directional
            //if (Input.GetKeyDown(KeyCode.W) && (Input.GetKeyDown(KeyCode.A)))
            //{
            //    moveDir = Vector3.forward + moveDir = Vector3.left;

                //    moveDir *= speed;
                //    movementBeingUsed = true;
                //}
        }

        //Rotation

        //rot = Input.mousePosition.x * rotSpeed * Time.deltaTime;
        //transform.eulerAngles = new Vector3(0, rot, 0) * rotSpeed;
        //transform.Rotate(new Vector3(0, rot, 0) * Time.deltaTime);
        
        moveDir.y -= gravity * Time.deltaTime;
        controller.Move(moveDir * speed * Time.deltaTime);
    }

    void rotation()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 100))
        {
            transform.LookAt(new Vector3(hit.point.x, transform.position.y, hit.point.z));
        }
    }
    void GetInput()
    {
       
            if (Input.GetMouseButtonDown(0))
            {
               Attacking();
            }

            if (Input.GetMouseButtonDown(1))
            {
               Shielding();
            }
    }

    void Attacking()
    {
        Console.WriteLine("attacking");
        StartCoroutine(AttackRoutine());
        
    }

    void Shielding()
    {
        Console.WriteLine("shielding");
        StartCoroutine(ShieldingRoutine());

    }

    //delay
    IEnumerator AttackRoutine()
    {
        yield return new WaitForSeconds(1);
    }

    //delay
    IEnumerator ShieldingRoutine()
    {
        yield return new WaitForSeconds(1);
    }


    //public class PlayerMovement : MonoBehaviour {
    //    void OnCollisionEnter(Collision collisionInfo) {
    //        Player player = new Player();
    //        if (collisionInfo.collider.tag == "Enemy") {
    //            //player.health = player.health - 1;
    //        }
    //    }
    //}

}
