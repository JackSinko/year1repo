﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class controllerTest : MonoBehaviour
{
    public float Speed = 5f;
    public float GroundDistance = 0.2f;
    public LayerMask Ground;

    private Rigidbody rBody;
    private Vector3 inputs = Vector3.zero;
    private bool isGrounded = true;
    private Transform groundChecker;

    
    void Start ()
    {
        rBody = GetComponent<Rigidbody>();
        groundChecker = transform.GetChild(0);
    }
	
	void Update ()
    {
        isGrounded = Physics.CheckSphere(groundChecker.position, GroundDistance, Ground, QueryTriggerInteraction.Ignore);

        inputs = Vector3.zero;
        inputs.x = Input.GetAxis("Horizontal");
        //inputs.y = Input.GetAxis("Vertical");
        transform.forward = inputs;
    }
    void FixedUpdate()
    {
        rBody.MovePosition(rBody.position + inputs * Speed * Time.fixedDeltaTime);
    }


    void movement()
    {
        bool w = Input.GetKey(KeyCode.W);

        if (w)
        {
            Vector3 tempVect = new Vector3(0, 0, 1);
            tempVect = tempVect * Speed * Time.deltaTime;
            rBody.MovePosition(transform.position + tempVect);

        }
        bool D = Input.GetKey(KeyCode.D);

        if (D)
        {
            Vector3 tempVect = new Vector3(0, 0, 1);
            tempVect = tempVect * Speed * Time.deltaTime;
            rBody.MovePosition(transform.position + tempVect);

        }
    }

    void Attacking()
    {
        Console.WriteLine("attacking");
        StartCoroutine(AttackRoutine());

    }

    void Shielding()
    {
        Console.WriteLine("shielding");
        StartCoroutine(ShieldingRoutine());

    }

    //delay
    IEnumerator AttackRoutine()
    {
        yield return new WaitForSeconds(1);
    }

    //delay
    IEnumerator ShieldingRoutine()
    {
        yield return new WaitForSeconds(1);
    }
}
